{literal}
    <style>
        .center {
            display: flex;
            justify-content: center;
            flex-direction: column;
            align-items: center;
        }

        .info__bird {
            border-bottom: #2f302d 2px solid;
            width: 30%;
        }
    </style>
{/literal}
<div id="manage-venues-blog">

    <div class="center">
        <p class="info__bird">
            id: {$bird.id}
        </p>
        <p class="info__bird">
            Bird Name: {$bird.name}
        </p>
        <p class="info__bird">
            Bird Descr: {$bird.desc}
        </p>
        <p class="info__bird">
            Bird Age: {$bird.age}
        </p>

    </div>
</div>
<div class="nowrap">
    <a href="{crmURL p='civicrm/birds/bird'}"
       class="action-item crm-hover-button">Back to List</a>
</div>

</div>