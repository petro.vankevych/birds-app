<?php

class CRM_Birds_BAO_Birds extends CRM_Birds_DAO_Birds
{

    /**
     * Adds params to Push Notification table
     *
     * @param $params
     *
     * @return \CRM_Core_DAO
     */
    public static function add(&$params)
    {
        $entity = new CRM_Birds_BAO_Birds();
        $entity->copyValues($params);
        return $entity->save();
    }

    /**
     * Creates new row in Push Notification table
     *
     * @param $params
     *
     * @return \CRM_Core_DAO
     */
    public static function &create(&$params)
    {
        $transaction = new self();

        if (!empty($params['id'])) {
            CRM_Utils_Hook::pre('edit', self::getEntityName(), $params['id'], $params);
        } else {
            CRM_Utils_Hook::pre('create', self::getEntityName(), NULL, $params);
        }

        $entityData = self::add($params);

        if (is_a($entityData, 'CRM_Core_Error')) {
            $transaction->rollback();
            return $entityData;
        }

        $transaction->commit();

        if (!empty($params['id'])) {
            CRM_Utils_Hook::post('edit', self::getEntityName(), $entityData->id, $entityData);
        } else {
            CRM_Utils_Hook::post('create', self::getEntityName(), $entityData->id, $entityData);
        }

        return $entityData;
    }

    /**
     * Deletes row in Push Notification table
     *
     * @param $id
     */
    public static function del($id)
    {
        $entity = new CRM_Birds_BAO_Birds();
        $entity->id = $id;
        $params = [];
        if ($entity->find(TRUE)) {
            CRM_Utils_Hook::pre('delete', self::getEntityName(), $entity->id, $params);
            $entity->delete();
            CRM_Utils_Hook::post('delete', self::getEntityName(), $entity->id, $entity);
        }
    }

    public static function getById($id)
    {
        $entity = new self();
        $entity->id = $id;
        if ($entity->find(true)) {
            return [
                'id' => $entity->id,
                'name' => $entity->birds_name,
                'desc' => $entity->birds_desc,
                'feed' => $entity->birds_feed,
            ];
        }

        return [];
    }

    /**
     * Builds query for receiving data
     *
     * @param string $returnValue
     *
     * @return \CRM_Utils_SQL_Select
     */
    private static function buildSelectQuery($returnValue = 'rows')
    {
        $query = CRM_Utils_SQL_Select::from(CRM_Birds_BAO_Birds::getTableName());

        if ($returnValue == 'rows') {
            $query->select('
        id,
        birds_name,
        birds_desc,
        birds_age,
        birds_feed,
        birds_like
      ');
        } else {
            if ($returnValue == 'count') {
                $query->select('COUNT(id)');
            }
        }

        return $query;
    }

    /**
     * Builds 'where' condition for query
     *
     * @param $query
     * @param array $params
     *
     * @return mixed
     */
    private static function buildWhereQuery($query, $params = [])
    {
        if (!empty($params['id'])) {
            $query->where('id = #id', ['id' => $params['id']]);
        }
        return $query;
    }

    /**
     * Gets all data
     *
     * @param array $params
     *
     * @return array
     */
    public static function getAll($params = [])
    {
        $query = self::buildWhereQuery(self::buildSelectQuery(), $params);
        return CRM_Core_DAO::executeQuery($query->toSQL())->fetchAll();
    }

}

// regexp - '/'[a-zA-Z0-9, -]{2,}