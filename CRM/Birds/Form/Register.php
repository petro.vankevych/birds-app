<?php

// use CRM_Birds_ExtensionUtil as E;

class CRM_Birds_Form_Register extends CRM_Core_Form {


    public function buildQuickForm() {
      parent::buildQuickForm();

      $this->addElement('text', 'birds_name',('Add birds name'));
      $this->addElement('text', 'birds_desc', ('Add birds_desc'));
      $this->addElement('text', 'birds_age', ('Add birds age'));
      $this->addElement('text', 'birds_feed', ('Add birds feed'));
      $this->addElement('checkbox', 'birds_like', ('Checker'));


      $buttons = [
        [
        'type' => 'submit',
        'name' => ('Register bird'),
        ]
      ];

      $this->addButtons($buttons);
    }

    public function setDefaultValues() {
        $defaults = [];
        $birdId = CRM_Utils_Request::retrieve('id', 'String');
        if ($this->getAction() == CRM_Core_Action::UPDATE) {
            $url = CRM_Utils_System::url('civicrm/brids/manage');
            try {
                $bird = CRM_Birds_BAO_Birds::getById($birdId);
            } catch (Exception $e) {
                CRM_Core_Error::statusBounce("Invalid id parameter.", $url);
            }

            $defaults['name'] = $bird['name'];
            $defaults['age'] = $bird['age'];
            $defaults['feed'] = $bird['feed'];
            $defaults['like'] = $bird['like'];
        }
        return $defaults;
    }


    public function postProcess() {
      $params = $this->exportValues();
      CRM_Birds_BAO_Birds::create($params);

      $title = "Status";
      $message = "Bird register successful!";
      $params = $this->exportValues();
      if (!empty(params['birds_name'])) {
          $message = "Bird not register! Please fill in all fields!";
        CRM_Core_Session::setStatus($message, $title, 'error');
      } else {
          CRM_Core_Session::setStatus($message, $title, 'Success');
          CRM_Utils_System::redirect(CRM_Utils_System::url('civicrm/birds/bird'));
      }
    }

    public function addRules() {
        $this->addFormRule([self::class, 'validateForm']);
    }

    public static function validateForm($values) {
        $errors = [];

        if (strlen($values['birds_name']) < 2) {
            $errors['birds_name'] = "The birds name is too short! Minimum number of characters 2!";
        }

        if (strlen($values['birds_name']) > 200) {
            $errors['birds_name'] = "The birds name is too long! Maximum number of characters 255!";
        }

        if (strlen($values['birds_desc']) == 0) {
            $errors['birds_desc'] = "The birds desc is too null! description cannot be empty == 0";
        }

        if (strlen($values['birds_age']) == 0) {
            $errors['birds_desc'] = "The birds age is too null! age cannot be empty == 0";
        }

        return empty($errors) ? TRUE : $errors;
    }
}

?>
