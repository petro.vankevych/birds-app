<?php

use CRM_CiviMobileAPI_ExtensionUtil as E;

/**
 * Provides token disabling functionality for CiviMobile application
 */
class CRM_Birds_Page_Bird extends CRM_Core_Page {

  public function run() {
    CRM_Utils_System::setTitle(E::ts("Bird List"));

    $myBirds = CRM_Birds_BAO_Birds::getAll();
    $this->assign('birds', $myBirds);
    return parent::run();
  }

}
