CREATE TABLE IF NOT EXISTS `civicrm_birds` (
    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
    `birds_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `birds_desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `birds_age` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `birds_feed` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
    `birds_like` tinyint(4) DEFAULT '1',
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
