<?php

//use CRM_Birds_ExstensionUtil as E;

class CRM_Birds_Form_DeleteBird extends CRM_Core_Form
{
    public function getProcess()
    {
        parent::preProcess();
        CRM_Utils_System::setTitle(('Delete Birds'));
    }

    public function buildQuickForm()
    {
        parent::buildQuickForm();
        $birdId = CRM_Utils_Request::retrieve('id', 'String');

        $buttons = [
            [
                'type' => 'submit',
                'name' => ('Delete'),
            ],
            [
                'type' => 'cancel',
                'name' => ('Cancel'),
                'js' => ['onclick' => " window.location.href='/?q=civicrm/birds/bird'; return false"
                ],
            ]
        ];

        $bird = new CRM_Birds_BAO_Birds();
        $birdInfo = $bird->getById($birdId);
        $birdName = $birdInfo['name'];

        $this->assign("birdName", $birdName);
        $this->add("hidden", 'birdId', $birdId);
        $this->addButtons($buttons);
    }

    public function cancelAction()
    {
        CRM_Utils_System::redirect(CRM_Utils_System::url('civicrm/birds/bird'));
        CRM_Utils_System::civiExit();
    }

    public function postProcess()
    {
        $params = $this->exportValues();
        $bird = new CRM_Birds_BAO_Birds();
        $birdInfo = $bird->getById($params['birdId']);
        $birdName = $birdInfo['name'];

        $this->assign('birdName', $birdName);
        $message = "The $birdName was successfully removed!";
        $title = "Status";
        if (isset($params['_qf_DeleteBird_submit'])) {
            $bird->del($params['birdId']);
            CRM_Core_Session::setStatus($message, $title, 'success');
            CRM_Utils_System::redirect(CRM_Utils_System::url('civicrm/birds/bird'));
            CRM_Utils_System::civiExit();
        }
    }
}

