<?php

//use CRM_Birds_ExstensionUtil as E;

class CRM_Birds_Form_EditBird extends CRM_Core_Form
{
    public function preProcess()
    {
        parent::preProcess();
    }

    public function buildQuickForm()
    {
        parent::buildQuickForm();

        $id = CRM_Utils_Request::retrieve('id', 'Integer');
        $this->addElement('text', 'birds_name', ('Add birds name'));
        $this->addElement('text', 'birds_desc', ('Add birds_desc'));
        $this->addElement('text', 'birds_age', ('Add birds age'));
        $this->addElement('text', 'birds_feed', ('Add birds feed'));
        $this->addElement('checkbox', 'birds_like', ('Checker'));


        $buttons = [
            [
                'type' => 'submit',
                'name' => ('Register bird'),
            ]
        ];

        $this->add("hidden", 'id', $id);
        $this->addButtons($buttons);
    }


    public function postProcess()
    {
        $params = $this->exportValues();
        CRM_Birds_BAO_Birds::create($params);

        $title = "Status";
        $message = "Edit successful!";
        $params = $this->exportValues();
        if (!empty(params['birds_name'])) {
            $message = "Edit do not edit! Please fill in all fields!";
            CRM_Core_Session::setStatus($message, $title, 'error');
        } else {
            CRM_Core_Session::setStatus($message, $title, 'Success');
            CRM_Utils_System::redirect(CRM_Utils_System::url('civicrm/birds/bird'));
        }
    }

    public function addRules()
    {
        $this->addFormRule([self::class, 'validateForm']);
    }

    public static function validateForm($values)
    {
        $errors = [];

        if (strlen($values['birds_name']) < 2) {
            $errors['birds_name'] = "The birds name is too short! Minimum number of characters 2!";
        }

        if (strlen($values['birds_name']) > 200) {
            $errors['birds_name'] = "The birds name is too long! Maximum number of characters 255!";
        }

        if (strlen($values['birds_desc']) == 0) {
            $errors['birds_desc'] = "The birds desc is too null! description cannot be empty == 0";
        }

        if (strlen($values['birds_age']) == 0) {
            $errors['birds_desc'] = "The birds age is too null! age cannot be empty == 0";
        }

        return empty($errors) ? TRUE : $errors;
    }
}

