{include file="CRM/common/jsortable.tpl"}
<div id="manage-venues-blog">
    <table class="row-highlight">
        <thead>
        <tr>
            <th class="sorting_disabled">
                {ts domain=com.agiliway.bird}Name{/ts}
            </th>
            <th class="sorting_disabled">
                {ts domain=com.agiliway.bird}Action{/ts}
            </th>
        </tr>
        </thead>
        {foreach from=$birds item=bird}
            <tr class="{cycle values="odd-row, even-row"}">
                <td>
                    {$bird.birds_name}
                </td>
                <td class="nowrap">
                    <a href="{crmURL p='civicrm/birds/delete' q='action=delete&id='}{$bird.id}"
                       class="action-item crm-hover-button">Delete</a>

                    <a href="{crmURL p='civicrm/birds/one' q='action=view&id='}{$bird.id}"
                       class="action-item crm-hover-button">View</a>

                    <a href="{crmURL p='civicrm/birds/edit' q='action=edit&id='}{$bird.id}"
                       class="action-item crm-hover-button">Edit</a>
                </td>
            </tr>
        {/foreach}
    </table>
</div>