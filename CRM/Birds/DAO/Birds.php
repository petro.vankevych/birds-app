<?php

use CRM_CiviMobileAPI_ExtensionUtil as E;
use CRM_Core_DAO;

class CRM_Birds_DAO_Birds extends CRM_Core_DAO
{

    /**
     * Static instance to hold the table name.
     *
     * @var string
     */
    static $_tableName = 'civicrm_birds';

    /**
     * Static entity name.
     *
     * @var string
     */
    static $entityName = 'DeleteBird.php';

    /**
     * Should CiviCRM log any modifications to this table in the civicrm_log
     * table.
     *
     * @var boolean
     */
    static $_log = TRUE;

    /**
     * Unique id of current row
     *
     * @var int
     */
    public $id;

    /**
     * Id of current contact
     *
     * @var string
     */
    public $birds_name;

    /**
     * Unique user identifier
     *
     * @var string
     */
    public $birds_desc;

    /**
     * Current user platform
     *
     * @var string
     */
    public $birds_age;

    /**
     * Date of creation
     *
     * @var string
     */
    public $birds_feed;

    /**
     * Date of last update
     *
     * @var string
     */
    public $birds_like;

    /**
     * Is row active
     *
     * @var int
     */
//    public $is_active;

    /**
     * Returns the names of this table
     *
     * @return string
     */
    static function getTableName()
    {
        return self::$_tableName;
    }

    /**
     * Returns entity name
     *
     * @return string
     */
    static function getEntityName()
    {
        return self::$entityName;
    }

    /**
     * Returns all the column names of this table
     *
     * @return array
     */
    static function &fields()
    {
        if (!isset(Civi::$statics[__CLASS__]['fields'])) {
            Civi::$statics[__CLASS__]['fields'] = [
                'id' => [
                    'name' => 'id',
                    'type' => CRM_Utils_Type::T_INT,
                    'title' => E::ts('id'),
                    'description' => 'id',
                    'required' => TRUE,
                    'import' => TRUE,
                    'where' => self::getTableName() . '.id',
                    'headerPattern' => '',
                    'dataPattern' => '',
                    'export' => TRUE,
                    'table_name' => self::getTableName(),
                    'entity' => self::getEntityName(),
                    'bao' => 'CRM_Birds_BAO_Birds',
                ],
                'birds_name' => [
                    'name' => 'birds_name',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => E::ts('DeleteBird.php Name'),
                    'description' => 'Contact id',
                    'required' => TRUE,
                    'import' => TRUE,
                    'where' => self::getTableName() . '.birds_name',
                    'headerPattern' => '',
                    'dataPattern' => '',
                    'export' => TRUE,
                    'table_name' => self::getTableName(),
                    'entity' => self::getEntityName(),
                    'bao' => 'CRM_Birds_BAO_Birds',
                ],
                'birds_desc' => [
                    'name' => 'birds_desc',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => E::ts('DeleteBird.php Desc'),
                    'description' => 'Token',
                    'required' => TRUE,
                    'import' => TRUE,
                    'where' => self::getTableName() . '.birds_desc',
                    'headerPattern' => '',
                    'dataPattern' => '',
                    'export' => TRUE,
                    'table_name' => self::getTableName(),
                    'entity' => self::getEntityName(),
                    'bao' => 'CRM_Birds_BAO_Birds',
                ],
                'birds_age' => [
                    'name' => 'birds_age',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => E::ts('DeleteBird.php Age'),
                    'description' => 'Year in Bird',
                    'required' => TRUE,
                    'import' => TRUE,
                    'where' => self::getTableName() . '.birds_age',
                    'headerPattern' => '',
                    'dataPattern' => '',
                    'export' => TRUE,
                    'table_name' => self::getTableName(),
                    'entity' => self::getEntityName(),
                    'bao' => 'CRM_Birds_BAO_Birds',
                ],
                'birds_feed' => [
                    'name' => 'birds_feed',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => E::ts('DeleteBird.php Feed'),
                    'description' => 'List of birds',
                    'required' => TRUE,
                    'import' => TRUE,
                    'where' => self::getTableName() . '.birds_feed',
                    'headerPattern' => '',
                    'dataPattern' => '',
                    'export' => TRUE,
                    'table_name' => self::getTableName(),
                    'entity' => self::getEntityName(),
                    'bao' => 'CRM_Birds_BAO_Birds',
                ],
                'birds_like' => [
                    'name' => 'birds_like',
                    'type' => CRM_Utils_Type::T_INT,
                    'title' => E::ts('Is active'),
                    'description' => 'Is active',
                    'required' => FALSE,
                    'import' => TRUE,
                    'where' => self::getTableName() . '.is_active',
                    'headerPattern' => '',
                    'dataPattern' => '',
                    'export' => TRUE,
                    'table_name' => self::getTableName(),
                    'entity' => self::getEntityName(),
                    'bao' => 'CRM_Birds_BAO_Birds',
                ],
            ];

            CRM_Core_DAO_AllCoreTables::invoke(__CLASS__, 'fields_callback', Civi::$statics[__CLASS__]['fields']);
        }

        return Civi::$statics[__CLASS__]['fields'];
    }

}
