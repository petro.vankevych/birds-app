<?php

class CRM_Birds_Page_BirdsApi extends CRM_Core_Page {
    public function run() {
        CRM_Utils_System::setTitle('Birds');

        $myBirds = civicore_api3('BirdsInfo', 'get all');
        $this->assing('dogs', $myBirds['values']);

        return parent::run();

    }
}