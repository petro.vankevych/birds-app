<?php

use CRM_Birds_ExtensionUtil as E;

function civicrm_api3_birds_get($params)
{
    $bird = new CRM_Birds_BAO_Birds();
    $myBirds = $bird->getAll();

    return civicrm_api3_create_success($myBirds);
}


function civicrm_api3_birds_create($params)
{
    $bird = new CRM_Birds_BAO_Birds();
    $myBirds = $bird->getAll();

    $params ['birds_like'] = (isset($params['birds_like'])) ? $params['birds_like'] : 1;

    $paramsForInsert = [
        'name' => $params['birds_name'],
        'desc' => $params['birds_desc'],
        'age' => $params['birds_age'],
        'feed' => $params['birds_feed'],
        'like' => $params['birds_like']
    ];

    CRM_Birds_BAO_Birds::create($paramsForInsert);

    $messages = 'success created';

    return civicrm_api3_create_success($messages ,$paramsForInsert);
}

function _civicrm_api3_birds_create_spec(&$params) {
    $params['birds_name'] = [
        'title' => 'name',
        'description' => E::ts('Name'),
        'api.required' => 1,
        'type' => CRM_Utils_Type::T_STRING
    ];
    $params['birds_desc'] = [
        'title' => 'Desc',
        'description' => E::ts('Desc'),
        'api.required' => 0,
        'type' => CRM_Utils_Type::T_STRING
    ];
    $params['birds_age'] = [
        'title' => 'Age',
        'description' => E::ts('Age'),
        'api.required' => 0,
        'type' => CRM_Utils_Type::T_STRING
    ];
    $params['birds_feed'] = [
        'title' => 'Feed',
        'description' => E::ts('Feed'),
        'api.required' => 0,
        'type' => CRM_Utils_Type::T_STRING
    ];
    $params['birds_like'] = [
        'title' => 'Like',
        'description' => E::ts('Like'),
        'api.required' => 0,
        'type' => CRM_Utils_Type::T_STRING
    ];
}

function civicrm_api3_birds_delete($params) {
    $bird = new CRM_Birds_BAO_Birds();
    $paramsId = $bird->getById($params['id']);
    $bird->del($params['id']);

    $messages = "success delete ". $params['id'];

    return civicrm_api3_create_success($messages, $params);
}

function _civicrm_api3_birds_delete_spec(&$params) {
    $params['id'] = [
        'title' => 'Id',
        'description' => E::ts('Enter id'),
        'api.required' => 1,
        'type' => CRM_Utils_Type::T_INT
    ];
}

