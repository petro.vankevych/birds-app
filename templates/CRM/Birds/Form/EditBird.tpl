{literal}
    <style>
        /*.form_bird {*/
        /*  display: flex;*/
        /*  justify-content: center;*/
        /*  align-items: center;*/
        /*}*/

        /*.bird__button {*/
        /*    display: flex;*/
        /*    justify-content: center;*/
        /*    align-items: center;*/
        /*}*/
    </style>
{/literal}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Page</title>
</head>
<body>

<div class="form_bird">
    <table class="form-layout-compressed">
        <tr class="crm-group-form-block-isReserved">
            <td class="label">{$form.birds_name.label}</td>
            <td class="view-value">{$form.birds_name.html}</td>
        </tr>
        <tr class="crm-group-form-block-isReserved">
            <td class="label">{$form.birds_desc.label}</td>
            <td class="view-value">{$form.birds_desc.html}</td>
        </tr>
        <tr class="crm-group-form-block-isReserved">
            <td class="label">{$form.birds_age.label|default}</td>
            <td class="view-value">{$form.birds_age.html}</td>

        <tr class="crm-group-form-block-isReserved">
            <td class="label">{$form.birds_feed.label}</td>
            <td class="view-value">{$form.birds_feed.html}</td>
        </tr>
        <tr class="crm-group-form-block-isReserved">
            <td class="label">{$form.birds_like.label}</td>
            <td class="view-value">{$form.birds_like.html}</td>
        </tr>
    </table>
</div>

<div class="crm-submit-buttons bird__button">
    {include file="CRM/common/formButtons.tpl" location="bottom"  multiple="multiple"}
</div>

</body>
</html>
