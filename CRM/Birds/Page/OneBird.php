<?php

use CRM_CiviMobileAPI_ExtensionUtil as E;

/**
 * Provides token disabling functionality for CiviMobile application
 */
class CRM_Birds_Page_OneBird extends CRM_Core_Page {

    public function run() {
        CRM_Utils_System::setTitle(E::ts("More Information"));

        $id = CRM_Utils_Request::retrieve('id', 'String');
        $bird = CRM_Birds_BAO_Birds::getById($id);
        $this->assign('bird', $bird);
        return parent::run();
    }

}
